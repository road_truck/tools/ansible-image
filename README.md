# Ansible Image
This repository automatically builds containers for using the `ansible` command line program. This image will be used by gitlab pipelines when performing infrastructure deployments.

## Getting Started
We uses a proprietary image and not the [official ansible docker image](https://hub.docker.com/u/ansible/) because it does not work as expected and there are no official images based on the alpine docker image. So we opted to create our docke image.

You can build this docker image locally by following the instructions:

### Prerequisites
As you can guess, you need to have `docker` and `git` installed on your computer. Once installed we can proceed to download the code and build the image.

### Building
If you've ever built a docker image, this is nothing special.

```bash
git clone git@gitlab.com:OrangeX/System/tools/ansible-image.git && cd ansible-image
docker build --build-arg ANSIBLE_VERSION=2.5.0 -t ansible-image:local .
```

Once the image is built, we will have in our premises an image whose name will be: `ansible-image:local`.

**Takes note that to build the ansible docker image the version of ansible to be built is passed to it as a building argument.**

### Running the tests
We can test run a ansible dummy command to verify that the image has been successfully constructed.
```bash
docker run --rm ansible-image:local ansible --version
ansible 2.5.0
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/local/lib/python3.6/site-packages/ansible
  executable location = /usr/local/bin/ansible
  python version = 3.6.4 (default, Jan 10 2018, 05:26:33) [GCC 5.3.0]
```

### Deepening
As mentioned above, this image is based on an image of `python:3-alpine`. The following layer diagram shows how this docker image is composed:

![Docker Layers](assets/layers.png "Docker Layers")

### Deployment
This image, as mentioned above, will be consumed by gitlab pipelines deploying infrastructure. This is why this image is deployed to gitlab's internal docker register (free of charge).

The name of the generated image is `registry.gitlab.com/orangex/system/tools/ansible-image:2.5.0`. The pipeline only runs when a new git tag is detected. This will be used to tag the docker image to generate. So it follows that there is only one git tag in conjunction with a version of the image we are building: version `2.5.0`

You can see the different versions of this image generated at the following link: [ansible-image/container_registry](https://gitlab.com/OrangeX/System/tools/ansible-image/container_registry)

## Acknowledgments
It is really important to keep up to date with these tools. So if a new version of ansible is released, it is convenient to create the respective git tag so that the appropriate version of the docker image is generated.

In this repository is attached the file `assets/layers.xml` which is the one used to draw the diagram of layers in `draw.io`. Please, if you make any changes to the layers of this docker image, modify the diagram as well.

## Authors

* **Juan Vicente Herrera** - *Initial work* - juanvicente.herrera@intelygenz.es
* **Ángel Barrera Sánchez** - *Refactorized* - angel.barrera@intelygenz.com
